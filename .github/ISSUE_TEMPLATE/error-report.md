---
name: Error Report
about: If you had an error , but you dont know anything , use this template
title: "[Error Report]"
labels: help wanted
assignees: ''

---

**What Happened**
Description.

**What are you doing when error occured?**
Description.

**Screenshots**
Images.

**Logs**
Logs File

**Addtional Context**
Any other words you want to say.

**Related issues and reports**
Links. [e.g. github.com/ProjectOpenLaunch/OpenLaunch/issues/1145141919810]
