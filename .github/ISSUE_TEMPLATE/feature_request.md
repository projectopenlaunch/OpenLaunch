---
name: Feature request
about: Express your brilliant ideas for Openlaunch!
title: "[Feature Request]"
labels: enhancement
assignees: ''

---

**What feature do you want?**
Describe the feature.

**Details about the feature**
Details.

**Additional Context**
What else do you want to say?
